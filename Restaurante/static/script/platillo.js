function eliminarPlatillo(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar el platillo seleccionado?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}

$(document).ready(function() {
  $("#nuevo_platillo").validate({
    rules: {
      "id_eu": {
        required: true
      },
      "nombre_eu": {
        required: true
      },
      "precio_eu": {
        required: true
      },
      "origen_eu": {
        required: true
      },
      "id_tipo": {
        required: true
      },
      "fotografia": {
        required: true
      }
    },
    messages: {
      "id_eu": {
        required: ""
      },
      "nombre_eu": {
        required: "Por favor, ingrese un nombre valido"
      },
      "precio_eu": {
        required: "Por favor, ingrese un precio valido"
      },
      "origen_eu": {
        required: "Por favor, ingrese un origen valido"
      },
      "id_tipo": {
        required: "Por favor, seleccione un tipo de platillo valido"
      },
      "fotografia": {
        required: "Por favor, seleccione un fotografia"
      }
    }
  });


  $("#nuevo_ediplatillo").validate({
    rules: {
      "id_eu": {
        required: true
      },
      "nombre_eu": {
        required: true
      },
      "precio_eu": {
        required: true
      },
      "origen_eu": {
        required: true
      },
      "id_tipo": {
        required: true
      }
    },
    messages: {
      "id_eu": {
        required: ""
      },
      "nombre_eu": {
        required: "Por favor, ingrese un nombre valido"
      },
      "precio_eu": {
        required: "Por favor, ingrese un precio valido"
      },
      "origen_eu": {
        required: "Por favor, ingrese un origen valido"
      },
      "id_tipo": {
        required: "Por favor, seleccione un tipo de platillo valido"
      }
    }
  });
  $('#tbl_platillo').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
          extend: 'pdfHtml5',
          text: '<i class="fas fa-file-pdf"></i> PDF',
          messageTop: 'LISTADO DE LOS PLATILLOS.'
      },
      {
        extend: 'print',
        text: '<i class="fas fa-print"></i> Imprimir'
      },
      {
        extend: 'csv',
        text: '<i class="fas fa-file-csv"></i> CSV'
      }
    ],
    language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            }
  } );
});
