function eliminarDetalle(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar el detalle seleccionado?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}

$(document).ready(function() {

  $("#nuevo_detalle").validate({
    rules: {
      "id_eu": {
        required: true
      },
      "id_platillo": {
        required: true
      },
      "fechaPedido_eu": {
        required: true
      },
      "cantidad_eu": {
        required: true
      },
      "total_eu": {
        required: true
      },
      "id_pedido": {
        required: true
      }
    },
    messages: {
      "id_eu": {
        required: ""
      },
      "id_platillo": {
        required: "Por favor, Seleccione un platillo"
      },
      "fechaPedido_eu": {
        required: "Por favor, Seleccione una fecha valida"
      },
      "cantidad_eu": {
        required: "Por favor, ingrese una cantidad valida"
      },
      "total_eu": {
        required: "Por favor, ingrese un total valido"
      },
      "id_pedido": {
        required: "Por favor, Seleccione el pedido"
      }
    }
  });
  $('#tbl_detalle').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
          extend: 'pdfHtml5',
          text: '<i class="fas fa-file-pdf"></i> PDF',
          messageTop: 'LISTADO DE LOS DETALLES.'
      },
      {
        extend: 'print',
        text: '<i class="fas fa-print"></i> Imprimir'
      },
      {
        extend: 'csv',
        text: '<i class="fas fa-file-csv"></i> CSV'
      }
    ],
    language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            }
  } );

  var today = new Date();
  var formattedDate = today.getFullYear() + '-' + (today.getMonth() + 1).toString().padStart(2, '0') + '-' + today.getDate().toString().padStart(2, '0');

  // Asigna la fecha actual al campo de fecha
  document.getElementById('fechaPedido_eu').value = formattedDate;
});
