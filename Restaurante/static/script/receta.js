function eliminarReceta(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar la receta seleccionada?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}
$(document).ready(function() {

  $("#nuevo_receta").validate({
    rules: {
      "id_eu": {
        required: true
      },
      "id_platillo": {
        required: true
      },
      "tiempoPreparacion_eu": {
        required: true
      },
      "descripcion_eu": {
        required: true
      },
      "id_ingrediente": {
        required: true
      }
    },
    messages: {
      "id_eu": {
        required: ""
      },
      "id_platillo": {
        required: "Por favor, seleccione un platillo"
      },
      "tiempoPreparacion_eu": {
        required: "Por favor, ingrese un tiempo valido"
      },
      "descripcion_eu": {
        required: "Por favor, ingrese una descripcion valida"
      },
      "id_ingrediente": {
        required: "Por favor, seleccione un ingrediente"
      }
    }
  });
  $('#tbl_receta').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
          extend: 'pdfHtml5',
          text: '<i class="fas fa-file-pdf"></i> PDF',
          messageTop: 'LISTADO DE LAS RECETAS DE PREPARACION.'
      },
      {
        extend: 'print',
        text: '<i class="fas fa-print"></i> Imprimir'
      },
      {
        extend: 'csv',
        text: '<i class="fas fa-file-csv"></i> CSV'
      }
    ],
    language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            }
  } );
});
