function eliminarClientes(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar al cliente seleccionado?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}
$(document).ready(function() {

  $("#nuevo_cliente").validate({
    rules: {
      "id_eu": {
        required: true
      },
      "nombre_eu": {
        required: true
      },
      "apellido_eu": {
        required: true
      },
      "telefono_eu": {
        required: true,
        minlength: 10,
        maxlength: 10
      },
      "direccion_eu": {
        required: true
      },
      "correo_eu": {
        required: true
      },
      "id_provincia": {
        required: true
      }
    },
    messages: {
      "id_eu": {
        required: ""
      },
      "nombre_eu": {
        required: "Por favor, ingrese un nombre valido"
      },
      "apellido_eu": {
        required: "Por favor, ingrese un apellido valido"
      },
      "telefono_eu": {
        required: "Por favor, ingrese un numero valido",
         minlength: "El número no debe tener menos de 10 dígitos",
         maxlength: "El número no debe tener más de 10 dígitos"
      },
      "direccion_eu": {
        required: "Por favor, ingrese una direccion valida"
      },
      "correo_eu": {
        required: "Por favor, ingrese un correo valido",
        email: "Error xxxx@gmail.com"
      },
      "id_provincia": {
        required: "Por favor, seleccione una provincia"
      }
    }
  });
  $('#tbl_clientes').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
          extend: 'pdfHtml5',
          text: '<i class="fas fa-file-pdf"></i> PDF',
          messageTop: 'LISTADO DE LOS CLIENTES.'
      },
      {
        extend: 'print',
        text: '<i class="fas fa-print"></i> Imprimir'
      },
      {
        extend: 'csv',
        text: '<i class="fas fa-file-csv"></i> CSV'
      }
    ],
    language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            }
  } );
});
