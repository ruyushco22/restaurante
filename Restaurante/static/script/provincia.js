function eliminarProvincia(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar la provincia seleccionada?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}
$(document).ready(function() {


    $("#nuevo_provincia").validate({
      rules: {
        "id_eu": {
          required: true
        },
        "nombre_eu": {
          required: true
        },
        "region_eu": {
          required: true
        },
        "capital_eu": {
          required: true
        },
        "fotografia": {
          required: true
        }
      },
      messages: {
        "id_eu": {
          required: "Por favor, ingrese el ID"
        },
        "nombre_eu": {
          required: "Por favor, ingrese el nombre"
        },
        "region_eu": {
          required: "Por favor, ingrese la región"
        },
        "capital_eu": {
          required: "Por favor, ingrese la capital"
        },
        "fotografia": {
          required: "Por favor, seleccione una fotografía"
        }
      }
    });

    $("#nuevo_ediprovincia").validate({
      rules: {
        "id_eu": {
          required: true
        },
        "nombre_eu": {
          required: true
        },
        "region_eu": {
          required: true
        },
        "capital_eu": {
          required: true
        }
      },
      messages: {
        "id_eu": {
          required: "Por favor, ingrese el ID"
        },
        "nombre_eu": {
          required: "Por favor, ingrese el nombre"
        },
        "region_eu": {
          required: "Por favor, ingrese la región"
        },
        "capital_eu": {
          required: "Por favor, ingrese la capital"
        }
      }
    });
    $('#tbl_provincias').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          {
              extend: 'pdfHtml5',
              text: '<i class="fas fa-file-pdf"></i> PDF',
              messageTop: 'LISTADO DE LAS PROVINCIAS.'
          },
          {
            extend: 'print',
            text: '<i class="fas fa-print"></i> Imprimir'
          },
          {
            extend: 'csv',
            text: '<i class="fas fa-file-csv"></i> CSV'
          }
      ],
      language: {
                  url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
              }
    } );
});
