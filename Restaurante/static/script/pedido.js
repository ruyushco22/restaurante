function eliminarPedido(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar el pedido seleccionado?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}
$(document).ready(function() {
  $("#nuevo_pedido").validate({
    rules: {
      "id_eu": {
        required: true
      },
      "pedido_eu": {
        required: true
      },
      "observaciones_eu": {
        required: true
      },
      "numeroPedidos_eu": {
        required: true
      },
      "id_cliente": {
        required: true
      }
    },
    messages: {
      "id_eu": {
        required: ""
      },
      "pedido_eu": {
        required: "Por favor, ingrese un pedido valido"
      },
      "observaciones_eu": {
        required: "Por favor, ingrese una observación valida"
      },
      "numeroPedidos_eu": {
        required: "Por favor, ingrese un numero de pedidos validos"
      },
      "id_cliente": {
        required: "Por favor, seleccione un cliente"
      }
    }
  });
  $('#tbl_pedido').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
          extend: 'pdfHtml5',
          text: '<i class="fas fa-file-pdf"></i> PDF',
          messageTop: 'LISTADO DE LOS PEDIDOS.'
      },
      {
        extend: 'print',
        text: '<i class="fas fa-print"></i> Imprimir'
      },
      {
        extend: 'csv',
        text: '<i class="fas fa-file-csv"></i> CSV'
      }
    ],
    language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            }
  } );

});
