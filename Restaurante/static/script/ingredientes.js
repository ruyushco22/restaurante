function eliminarIngredientes(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar el ingrediente seleccionado?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}

$(document).ready(function() {

  $("#nuevo_ingrediente").validate({
    rules: {
      "id_eu": {
        required: true
      },
      "nombre_eu": {
        required: true
      },
      "descripcion_eu": {
        required: true
      },
      "fechaExpiracion_eu": {
        required: true
      },
      "fotografia": {
        required: true
      }
    },
    messages: {
      "id_eu": {
        required: ""
      },
      "nombre_eu": {
        required: "Por favor, ingrese un nombre valido"
      },
      "descripcion_eu": {
        required: "Por favor, ingrese una descripción valida"
      },
      "fechaExpiracion_eu": {
        required: "Por favor, seleccione una fecha de expiracion"
      },
      "fotografia": {
        required: "Por favor, seleccione una fotografia"
      }
    }
  });

  $("#nuevo_edingrediente").validate({
    rules: {
      "id_eu": {
        required: true
      },
      "nombre_eu": {
        required: true
      },
      "descripcion_eu": {
        required: true
      },
      "fechaExpiracion_eu": {
        required: true
      }
    },
    messages: {
      "id_eu": {
        required: ""
      },
      "nombre_eu": {
        required: "Por favor, ingrese un nombre valido"
      },
      "descripcion_eu": {
        required: "Por favor, ingrese una descripción valida"
      },
      "fechaExpiracion_eu": {
        required: "Por favor, seleccione una fecha de expiracion"
      }
    }
  });
  $('#tbl_ingredientes').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
          extend: 'pdfHtml5',
          text: '<i class="fas fa-file-pdf"></i> PDF',
          messageTop: 'LISTADO DE LOS INGREDIENTES.'
      },
      {
        extend: 'print',
        text: '<i class="fas fa-print"></i> Imprimir'
      },
      {
        extend: 'csv',
        text: '<i class="fas fa-file-csv"></i> CSV'
      }
    ],
    language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            }
  } );
});
