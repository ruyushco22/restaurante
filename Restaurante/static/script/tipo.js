function eliminarTipo(url){
  iziToast.question({
      timeout: 15000,
      close: true,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'CONFIRMACIÓN',
      message: '¿Está seguro de eliminar el tipo seleccionado?',
      position: 'center',
      buttons: [
          ['<button><b>SI</b></button>', function (instance, toast) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
              window.location.href=url;
          }, true],
          ['<button>NO</button>', function (instance, toast) {

              instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

          }],
      ]
  });
}

$(document).ready(function() {

  $("#nuevo_tipo").validate({
    rules: {
      "id_eu": {
        required: true
      },
      "tipo_eu": {
        required: true
      },
      "descripcion_eu": {
        required: true
      },
      "recomendacion_eu": {
        required: true
      }
    },
    messages: {
      "id_eu": {
        required: ""
      },
      "tipo_eu": {
        required: "Por favor, ingrese un nombre valido"
      },
      "descripcion_eu": {
        required: "Por favor, ingrese una descripcion valida"
      },
      "recomendacion_eu": {
        required: "Por favor, ingrese una recomendacion valida"
      }
    }
  });

  $("#nuevo_editipo").validate({
    rules: {
      "id_eu": {
        required: true
      },
      "tipo_eu": {
        required: true
      },
      "descripcion_eu": {
        required: true
      },
      "recomendacion_eu": {
        required: true
      }
    },
    messages: {
      "id_eu": {
        required: ""
      },
      "tipo_eu": {
        required: "Por favor, ingrese un nombre valido"
      },
      "descripcion_eu": {
        required: "Por favor, ingrese una descripcion valida"
      },
      "recomendacion_eu": {
        required: "Por favor, ingrese una recomendacion valida"
      }
    }
  });
  $('#tbl_tipo').DataTable( {
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'pdfHtml5',
            text: '<i class="fas fa-file-pdf"></i> PDF',
            messageTop: 'LISTADO DE LOS TIPOS DE PLATILLOS.'
        },
        {
          extend: 'print',
          text: '<i class="fas fa-print"></i> Imprimir'
        },
        {
          extend: 'csv',
          text: '<i class="fas fa-file-csv"></i> CSV'
        }
    ],
    language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            }
  } );
});
