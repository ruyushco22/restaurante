from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Provincia
from .models import Clientes
from .models import Pedido
from .models import Tipo
from .models import Platillo
from .models import Detalle
from .models import Ingredientes
from .models import Receta
# DATOS DE PROVINCIA
def listadoProvincia(request):
    provinciaBdd = Provincia.objects.all()
    return render(request, 'listadoProvincia.html', {'provincia': provinciaBdd})
def guardarProvincia(request):
    nombre_eu=request.POST["nombre_eu"]
    region_eu=request.POST["region_eu"]
    capital_eu=request.POST["capital_eu"]
    fotografia=request.FILES.get("fotografia")
    nuevoProvincia=Provincia.objects.create(nombre_eu=nombre_eu,region_eu=region_eu,capital_eu=capital_eu,fotografia=fotografia)
    messages.success(request, 'Provincia Guardada Exitosamente')
    return redirect('/')
def eliminarProvincia(request,id):
    provinciaEliminar=Provincia.objects.get(id_eu=id)
    provinciaEliminar.delete()
    messages.success(request, 'Provincia Eliminada Exitosamente')
    return redirect ('/')
def editarProvincia(request,id):
    provinciaEditar=Provincia.objects.get(id_eu=id)
    return render(request,'editarProvincia.html',{'provincia':provinciaEditar})
def actualizarProvincia(request):
    if request.method == 'POST':
        id_eu=request.POST["id_eu"]
        nombre_eu=request.POST["nombre_eu"]
        region_eu=request.POST["region_eu"]
        capital_eu=request.POST["capital_eu"]
        #Insertando datos mediante el ORM de DJANGO
        provinciaEditar=Provincia.objects.get(id_eu=id_eu)
        provinciaEditar.nombre_eu=nombre_eu
        provinciaEditar.region_eu=region_eu
        provinciaEditar.capital_eu=capital_eu
        if 'fotografia' in request.FILES:
            provinciaEditar.fotografia = request.FILES['fotografia']
        provinciaEditar.save()
        messages.success(request, 'Provincia Actualizada Exitosamente')
        return redirect('/')
    else:

        pass

# DATOS DE CLIENTES /////////////////////////////////////////////////////////////////////////////////////////////////
def listadoClientes(request):
    clientesBdd = Clientes.objects.all()
    provinciaBdd = Provincia.objects.all()
    return render(request, 'listadoClientes.html', {'clientes': clientesBdd,'provincia': provinciaBdd})
def guardarClientes(request):
    id_provincia=request.POST["id_provincia"]
    provinciaSelecionado=Provincia.objects.get(id_eu=id_provincia)
    nombre_eu=request.POST["nombre_eu"]
    apellido_eu=request.POST["apellido_eu"]
    telefono_eu=request.POST["telefono_eu"]
    direccion_eu=request.POST["direccion_eu"]
    correo_eu=request.POST["correo_eu"]
    nuevoClientes=Clientes.objects.create(nombre_eu=nombre_eu,apellido_eu=apellido_eu,telefono_eu=telefono_eu,direccion_eu=direccion_eu,correo_eu=correo_eu,provincia_eu=provinciaSelecionado)
    messages.success(request, 'Cliente Guardado Exitosamente')
    return redirect('cliente')
def eliminarClientes(request,id):
    clientesEliminar=Clientes.objects.get(id_eu=id)
    clientesEliminar.delete()
    messages.success(request, 'Cliente Eliminado Exitosamente')
    return redirect ('cliente')
def editarClientes(request,id):
    clientesEditar = Clientes.objects.get(id_eu=id)
    provinciaBdd = Provincia.objects.all()
    return render(request, 'editarClientes.html', {'clientes': clientesEditar,'provincia': provinciaBdd})
def actualizarClientes(request):
    id_eu=request.POST["id_eu"]
    id_provincia=request.POST["id_provincia"]
    provinciaSelecionado=Provincia.objects.get(id_eu=id_provincia)
    nombre_eu=request.POST["nombre_eu"]
    apellido_eu=request.POST["apellido_eu"]
    telefono_eu=request.POST["telefono_eu"]
    direccion_eu=request.POST["direccion_eu"]
    correo_eu=request.POST["correo_eu"]
    #Insertando datos mediante el ORM de DJANGO
    clientesEditar=Clientes.objects.get(id_eu=id_eu)
    clientesEditar.provincia_eu=provinciaSelecionado
    clientesEditar.nombre_eu=nombre_eu
    clientesEditar.apellido_eu=apellido_eu
    clientesEditar.telefono_eu=telefono_eu
    clientesEditar.direccion_eu=direccion_eu
    clientesEditar.correo_eu=correo_eu
    clientesEditar.save()
    messages.success(request,'Cliente Actualizado Exitosamente')
    return redirect('cliente')


def listadoPedido(request):
    pedidoBdd = Pedido.objects.all()
    clientesBdd = Clientes.objects.all()
    return render(request, 'listadoPedido.html', {'pedido': pedidoBdd,'clientes': clientesBdd})
def guardarPedido(request):
    id_cliente=request.POST["id_cliente"]
    clienteSelecionado=Clientes.objects.get(id_eu=id_cliente)
    pedido_eu=request.POST["pedido_eu"]
    observaciones_eu=request.POST["observaciones_eu"]
    numeroPedidos_eu=request.POST["numeroPedidos_eu"]
    nuevoReceta=Pedido.objects.create(pedido_eu=pedido_eu,observaciones_eu=observaciones_eu,numeroPedidos_eu=numeroPedidos_eu,clientes_eu=clienteSelecionado)
    messages.success(request, 'Pedido Guardado Exitosamente')
    return redirect('pedido')
def eliminarPedido(request,id):
    pedidoEliminar=Pedido.objects.get(id_eu=id)
    pedidoEliminar.delete()
    messages.success(request, 'Pedido Eliminado Exitosamente')
    return redirect ('pedido')
def editarPedido(request,id):
    pedidoEditar = Pedido.objects.get(id_eu=id)
    clientesBdd = Clientes.objects.all()
    return render(request, 'editarPedido.html', {'pedido': pedidoEditar,'clientes': clientesBdd})

def actualizarPedido(request):
    id_eu=request.POST["id_eu"]
    id_cliente=request.POST["id_cliente"]
    clienteSelecionado=Clientes.objects.get(id_eu=id_cliente)
    pedido_eu=request.POST["pedido_eu"]
    observaciones_eu=request.POST["observaciones_eu"]
    numeroPedidos_eu=request.POST["numeroPedidos_eu"]
    #Insertando datos mediante el ORM de DJANGO
    pedidoEditar=Pedido.objects.get(id_eu=id_eu)
    pedidoEditar.clientes_eu=clienteSelecionado
    pedidoEditar.pedido_eu=pedido_eu
    pedidoEditar.observaciones_eu=observaciones_eu
    pedidoEditar.numeroPedidos_eu=numeroPedidos_eu
    pedidoEditar.save()
    messages.success(request,'Pedido Actualizado Exitosamente')
    return redirect('pedido')

def listadoTipo(request):
    tipoBdd = Tipo.objects.all()
    return render(request, 'listadoTipo.html', {'tipo': tipoBdd})
def guardarTipo(request):
    tipo_eu=request.POST["tipo_eu"]
    descripcion_eu=request.POST["descripcion_eu"]
    recomendacion_eu=request.POST["recomendacion_eu"]
    fotografia=request.FILES.get("fotografia")
    nuevoTipo=Tipo.objects.create(tipo_eu=tipo_eu,descripcion_eu=descripcion_eu,recomendacion_eu=recomendacion_eu,fotografia=fotografia)
    messages.success(request, 'Tipo Guardado Exitosamente')
    return redirect('tipo')
def eliminarTipo(request,id):
    tipoEliminar=Tipo.objects.get(id_eu=id)
    tipoEliminar.delete()
    messages.success(request, 'Tipo Eliminado Exitosamente')
    return redirect ('tipo')
def editarTipo(request,id):
    tipoEditar=Tipo.objects.get(id_eu=id)
    return render(request,'editarTipo.html',{'tipo':tipoEditar})
def actualizarTipo(request):
    if request.method == 'POST':
        id_eu=request.POST["id_eu"]
        tipo_eu=request.POST["tipo_eu"]
        descripcion_eu=request.POST["descripcion_eu"]
        recomendacion_eu=request.POST["recomendacion_eu"]
        #Insertando datos mediante el ORM de DJANGO
        tipoEditar=Tipo.objects.get(id_eu=id_eu)
        tipoEditar.tipo_eu=tipo_eu
        tipoEditar.descripcion_eu=descripcion_eu
        tipoEditar.recomendacion_eu=recomendacion_eu
        if 'fotografia' in request.FILES:
            tipoEditar.fotografia = request.FILES['fotografia']
        tipoEditar.save()
        messages.success(request, 'Tipo Actualizado Exitosamente')
        return redirect('tipo')
    else:

        pass

def listadoPlatillo(request):
    platilloBdd = Platillo.objects.all()
    tipoBdd = Tipo.objects.all()
    return render(request, 'listadoPlatillo.html', {'platillo': platilloBdd,'tipo': tipoBdd})
def guardarPlatillo(request):
    id_tipo=request.POST["id_tipo"]
    tipoSelecionado=Tipo.objects.get(id_eu=id_tipo)
    nombre_eu=request.POST["nombre_eu"]
    precio_eu=request.POST["precio_eu"]
    origen_eu=request.POST["origen_eu"]
    fotografia=request.FILES.get("fotografia")
    nuevoPlatillo=Platillo.objects.create(nombre_eu=nombre_eu,precio_eu=precio_eu,origen_eu=origen_eu,tipo=tipoSelecionado,fotografia=fotografia)
    messages.success(request, 'Platillo Guardado Exitosamente')
    return redirect('platillo')
def eliminarPlatillo(request,id):
    platilloEliminar=Platillo.objects.get(id_eu=id)
    platilloEliminar.delete()
    messages.success(request, 'Platillo Eliminado Exitosamente')
    return redirect ('platillo')
def editarPlatillo(request,id):
    platilloEditar = Platillo.objects.get(id_eu=id)
    tipoBdd = Tipo.objects.all()
    return render(request, 'editarPlatillo.html', {'platillo': platilloEditar,'tipo': tipoBdd})

def actualizarPlatillo(request):
    if request.method == 'POST':
        id_eu=request.POST["id_eu"]
        id_tipo=request.POST["id_tipo"]
        tipoSelecionado=Tipo.objects.get(id_eu=id_tipo)
        nombre_eu=request.POST["nombre_eu"]
        precio_eu=request.POST["precio_eu"]
        origen_eu=request.POST["origen_eu"]
        #Insertando datos mediante el ORM de DJANGO
        platilloEditar=Platillo.objects.get(id_eu=id_eu)
        platilloEditar.tipo_eu=tipoSelecionado
        platilloEditar.nombre_eu=nombre_eu
        platilloEditar.precio_eu=precio_eu
        platilloEditar.origen_eu=origen_eu
        if 'fotografia' in request.FILES:
            platilloEditar.fotografia = request.FILES['fotografia']
        platilloEditar.save()
        messages.success(request, 'Platillo Actualizado Exitosamente')
        return redirect('platillo')
    else:

        pass


def listadoDetalle(request):
    detalleBdd = Detalle.objects.all()
    pedidoBdd = Pedido.objects.all()
    platilloBdd = Platillo.objects.all()
    return render(request, 'listadoDetalle.html', {'detalle': detalleBdd,'pedido': pedidoBdd,'platillo': platilloBdd})
def guardarDetalle(request):
    id_platillo=request.POST["id_platillo"]
    platilloSelecionado=Platillo.objects.get(id_eu=id_platillo)
    id_pedido=request.POST["id_pedido"]
    pedidoSelecionado=Pedido.objects.get(id_eu=id_pedido)
    fechaPedido_eu=request.POST["fechaPedido_eu"]
    cantidad_eu=request.POST["cantidad_eu"]
    total_eu=request.POST["total_eu"]
    nuevoDetalle=Detalle.objects.create(fechaPedido_eu=fechaPedido_eu,cantidad_eu=cantidad_eu,total_eu=total_eu,platillo_eu=platilloSelecionado,pedido_eu=pedidoSelecionado)
    messages.success(request, 'Detalle Guardado Exitosamente')
    return redirect('detalle')
def eliminarDetalle(request,id):
    detalleEliminar=Detalle.objects.get(id_eu=id)
    detalleEliminar.delete()
    messages.success(request, 'Detalle Eliminado Exitosamente')
    return redirect ('detalle')

def editarDetalle(request,id):
    detalleEditar = Detalle.objects.get(id_eu=id)
    pedidoBdd = Pedido.objects.all()
    platilloBdd = Platillo.objects.all()
    return render(request, 'editarDetalle.html', {'detalle': detalleEditar,'pedido': pedidoBdd,'platillo': platilloBdd})
def actualizarDetalle(request):
    id_eu=request.POST["id_eu"]
    id_platillo=request.POST["id_platillo"]
    platilloSelecionado=Platillo.objects.get(id_eu=id_platillo)
    id_pedido=request.POST["id_pedido"]
    pedidoSelecionado=Pedido.objects.get(id_eu=id_pedido)
    fechaPedido_eu=request.POST["fechaPedido_eu"]
    cantidad_eu=request.POST["cantidad_eu"]
    total_eu=request.POST["total_eu"]
    #Insertando datos mediante el ORM de DJANGO
    detalleEditar=Detalle.objects.get(id_eu=id_eu)
    detalleEditar.pedido_eu=pedidoSelecionado
    detalleEditar.platillo_eu=platilloSelecionado
    detalleEditar.fechaPedido_eu=fechaPedido_eu
    detalleEditar.cantidad_eu=cantidad_eu
    detalleEditar.total_eu=total_eu
    detalleEditar.save()
    messages.success(request,
      'Detalle Actualizado Exitosamente')
    return redirect('detalle')


def listadoIngredientes(request):
    ingredientesBdd = Ingredientes.objects.all()
    return render(request, 'listadoIngredientes.html', {'ingredientes': ingredientesBdd})
def guardarIngredientes(request):
    nombre_eu=request.POST["nombre_eu"]
    descripcion_eu=request.POST["descripcion_eu"]
    fechaExpiracion_eu=request.POST["fechaExpiracion_eu"]
    fotografia=request.FILES.get("fotografia")
    nuevoIngredientes=Ingredientes.objects.create(nombre_eu=nombre_eu,descripcion_eu=descripcion_eu,fechaExpiracion_eu=fechaExpiracion_eu,fotografia=fotografia)
    messages.success(request, 'Ingrediente Guardad Exitosamente')
    return redirect('ingrediente')
def eliminarIngredientes(request,id):
    ingredientesEliminar=Ingredientes.objects.get(id_eu=id)
    ingredientesEliminar.delete()
    messages.success(request, 'Ingrediente Eliminado Exitosamente')
    return redirect ('ingrediente')
def editarIngredientes(request,id):
    ingredientesEditar=Ingredientes.objects.get(id_eu=id)
    return render(request,'editarIngredientes.html',{'ingredientes':ingredientesEditar})
def actualizarIngredientes(request):
    if request.method == 'POST':
        id_eu=request.POST["id_eu"]
        nombre_eu=request.POST["nombre_eu"]
        descripcion_eu=request.POST["descripcion_eu"]
        fechaExpiracion_eu=request.POST["fechaExpiracion_eu"]
        #Insertando datos mediante el ORM de DJANGO
        ingredientesEditar=Ingredientes.objects.get(id_eu=id_eu)
        ingredientesEditar.nombre_eu=nombre_eu
        ingredientesEditar.descripcion_eu=descripcion_eu
        ingredientesEditar.fechaExpiracion_eu=fechaExpiracion_eu
        if 'fotografia' in request.FILES:
            ingredientesEditar.fotografia = request.FILES['fotografia']
        ingredientesEditar.save()
        messages.success(request, 'Ingrediente Actualizado Exitosamente')
        return redirect('ingrediente')
    else:

        pass


def listadoReceta(request):
    recetaBdd = Receta.objects.all()
    platilloBdd = Platillo.objects.all()
    ingredientesBdd = Ingredientes.objects.all()
    return render(request, 'listadoReceta.html', {'receta': recetaBdd,'platillo': platilloBdd,'ingredientes': ingredientesBdd})
def guardarReceta(request):
    id_platillo=request.POST["id_platillo"]
    platilloSelecionado=Platillo.objects.get(id_eu=id_platillo)
    id_ingrediente=request.POST["id_ingrediente"]
    ingredienteSelecionado=Ingredientes.objects.get(id_eu=id_ingrediente)
    tiempoPreparacion_eu=request.POST["tiempoPreparacion_eu"]
    descripcion_eu=request.POST["descripcion_eu"]
    nuevoReceta=Receta.objects.create(tiempoPreparacion_eu=tiempoPreparacion_eu,descripcion_eu=descripcion_eu,platillo_eu=platilloSelecionado,ingredientes_eu=ingredienteSelecionado)
    messages.success(request, 'Receta Guardada Exitosamente')
    return redirect('receta')
def eliminarReceta(request,id):
    recetaEliminar=Receta.objects.get(id_eu=id)
    recetaEliminar.delete()
    messages.success(request, 'Receta Eliminada Exitosamente')
    return redirect ('receta')
def editarReceta(request,id):
    recetaEditar = Receta.objects.get(id_eu=id)
    platilloBdd = Platillo.objects.all()
    ingredientesBdd = Ingredientes.objects.all()
    return render(request, 'editarReceta.html', {'receta': recetaEditar,'platillo': platilloBdd,'ingredientes': ingredientesBdd})

def actualizarReceta(request):
    id_eu=request.POST["id_eu"]

    id_platillo=request.POST["id_platillo"]
    platilloSelecionado=Platillo.objects.get(id_eu=id_platillo)
    id_ingrediente=request.POST["id_ingrediente"]
    ingredienteSelecionado=Ingredientes.objects.get(id_eu=id_ingrediente)
    tiempoPreparacion_eu=request.POST["tiempoPreparacion_eu"]
    descripcion_eu=request.POST["descripcion_eu"]
    #Insertando datos mediante el ORM de DJANGO
    recetaEditar=Receta.objects.get(id_eu=id_eu)
    recetaEditar.platillo_eu=platilloSelecionado
    recetaEditar.ingredientes_eu=ingredienteSelecionado
    recetaEditar.tiempoPreparacion_eu=tiempoPreparacion_eu
    recetaEditar.descripcion_eu=descripcion_eu
    recetaEditar.save()
    messages.success(request,
      'Receta Actualizada Exitosamente')
    return redirect('receta')
