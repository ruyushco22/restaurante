from django.db import models

# Create your models here.
class Provincia(models.Model):
    id_eu=models.AutoField(primary_key=True)
    nombre_eu=models.CharField(max_length=150)
    region_eu=models.CharField(max_length=150)
    capital_eu=models.CharField(max_length=150)
    fotografia=models.FileField(upload_to='provincias',null=True,blank=True)

class Clientes(models.Model):
    id_eu=models.AutoField(primary_key=True)
    nombre_eu=models.CharField(max_length=150)
    apellido_eu=models.CharField(max_length=150)
    telefono_eu=models.CharField(max_length=150)
    direccion_eu=models.TextField()
    correo_eu=models.EmailField()
    provincia_eu=models.ForeignKey(Provincia,null=True,blank=True,on_delete=models.SET_NULL)

class Pedido(models.Model):
    id_eu=models.AutoField(primary_key=True)
    pedido_eu=models.CharField(max_length=150)
    observaciones_eu=models.CharField(max_length=150)
    numeroPedidos_eu=models.CharField(max_length=150)
    clientes_eu=models.ForeignKey(Clientes,null=True,blank=True,on_delete=models.SET_NULL)

class Tipo(models.Model):
    id_eu=models.AutoField(primary_key=True)
    tipo_eu=models.CharField(max_length=150)
    descripcion_eu=models.CharField(max_length=150)
    recomendacion_eu=models.CharField(max_length=150)
    fotografia=models.FileField(upload_to='tipo',null=True,blank=True)

class Platillo(models.Model):
    id_eu=models.AutoField(primary_key=True)
    nombre_eu=models.CharField(max_length=150)
    precio_eu=models.CharField(max_length=150)
    origen_eu=models.CharField(max_length=150)
    tipo=models.ForeignKey(Tipo,null=True,blank=True,on_delete=models.SET_NULL)
    fotografia=models.FileField(upload_to='platillo',null=True,blank=True)

class Detalle(models.Model):
    id_eu=models.AutoField(primary_key=True)
    fechaPedido_eu=models.CharField(max_length=150)
    cantidad_eu=models.CharField(max_length=150)
    total_eu=models.CharField(max_length=150)
    pedido_eu=models.ForeignKey(Pedido,null=True,blank=True,on_delete=models.SET_NULL)
    platillo_eu=models.ForeignKey(Platillo,null=True,blank=True,on_delete=models.SET_NULL)

class Ingredientes(models.Model):
    id_eu=models.AutoField(primary_key=True)
    nombre_eu=models.CharField(max_length=150)
    descripcion_eu=models.CharField(max_length=150)
    fechaExpiracion_eu=models.CharField(max_length=150)
    fotografia=models.FileField(upload_to='ingredientes',null=True,blank=True)

class Receta(models.Model):
    id_eu=models.AutoField(primary_key=True)
    tiempoPreparacion_eu=models.CharField(max_length=150)
    descripcion_eu=models.CharField(max_length=150)
    platillo_eu=models.ForeignKey(Platillo,null=True,blank=True,on_delete=models.SET_NULL)
    ingredientes_eu=models.ForeignKey(Ingredientes,null=True,blank=True,on_delete=models.SET_NULL)
