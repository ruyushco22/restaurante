from django.apps import AppConfig


class RestauranteeuConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.RestauranteEU'
