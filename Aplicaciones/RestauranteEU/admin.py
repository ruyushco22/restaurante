from django.contrib import admin
from .models import Provincia

# Registra los modelos en el panel de administración
admin.site.register(Provincia)
