from django.urls import path
from . import views

urlpatterns = [
    path('', views.listadoProvincia),
    path('guardarProvincia/',views.guardarProvincia),
    path('eliminarProvincia/<id>',views.eliminarProvincia),
    path('editarProvincia/<id>',views.editarProvincia),
    path('actualizarProvincia/', views.actualizarProvincia),

    path('cliente',views.listadoClientes, name="cliente"),
    path('guardarClientes/',views.guardarClientes),
    path('eliminarClientes/<id>',views.eliminarClientes),
    path('editarClientes/<id>',views.editarClientes),
    path('actualizarClientes/', views.actualizarClientes),

    path('pedido',views.listadoPedido, name="pedido"),
    path('guardarPedido/',views.guardarPedido),
    path('eliminarPedido/<id>',views.eliminarPedido),
    path('editarPedido/<id>',views.editarPedido),
    path('actualizarPedido/', views.actualizarPedido),

    path('tipo',views.listadoTipo, name="tipo"),
    path('guardarTipo/',views.guardarTipo),
    path('eliminarTipo/<id>',views.eliminarTipo),
    path('editarTipo/<id>',views.editarTipo),
    path('actualizarTipo/', views.actualizarTipo),

    path('platillo',views.listadoPlatillo, name="platillo"),
    path('guardarPlatillo/',views.guardarPlatillo),
    path('eliminarPlatillo/<id>',views.eliminarPlatillo),
    path('editarPlatillo/<id>',views.editarPlatillo),
    path('actualizarPlatillo/', views.actualizarPlatillo),

    path('detalle',views.listadoDetalle, name="detalle"),
    path('guardarDetalle/',views.guardarDetalle),
    path('eliminarDetalle/<id>',views.eliminarDetalle),
    path('editarDetalle/<id>',views.editarDetalle),
    path('actualizarDetalle/',views.actualizarDetalle),

    path('ingrediente',views.listadoIngredientes, name="ingrediente"),
    path('guardarIngredientes/',views.guardarIngredientes),
    path('eliminarIngredientes/<id>',views.eliminarIngredientes),
    path('editarIngredientes/<id>',views.editarIngredientes),
    path('actualizarIngredientes/',views.actualizarIngredientes),

    path('receta',views.listadoReceta, name="receta"),
    path('guardarReceta/',views.guardarReceta),
    path('eliminarReceta/<id>',views.eliminarReceta),
    path('editarReceta/<id>',views.editarReceta),
    path('actualizarReceta/',views.actualizarReceta)
]
